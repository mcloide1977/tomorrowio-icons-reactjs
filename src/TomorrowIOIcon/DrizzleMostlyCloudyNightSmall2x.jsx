
/**
 * @author Cristiano Diniz da Silva <@mcloide1977>
 */

import React from "react"
import TomorrowIOIcon from "./TomorrowIOIcon";

/**
 * Will create an instance of the DrizzleMostlyCloudyNightSmall2x TomorrowIO Icon JSX.Element that will create an image
 * tag and return to the as rendered.
 *
 * @package TomorrowIOIcon
 */
class DrizzleMostlyCloudyNightSmall2x extends React.Component {

    /**
     * @inheritDoc
     * @returns {JSX.Element}
     */
    render() {
        return (
            <TomorrowIOIcon
                imgUrl="./src/lib/V2_icons/small/png/42051_drizzle_mostly_cloudy_small@2x.png"
                imgWidth={this.props.imgWidth}
                imgHeight={this.props.imgHeight}
                imgAlt={this.props.imgAlt}
                imgStyle={this.props.imgStyle}
                imgId="tomorrow-io-v2-icons-42051-drizzle-mostly-cloudy-small@2x"
            />
        );
    }
}

export default DrizzleMostlyCloudyNightSmall2x;
