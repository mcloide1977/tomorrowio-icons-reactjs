
/**
 * @author Cristiano Diniz da Silva <@mcloide1977>
 */

import React from "react"
import TomorrowIOIcon from "./TomorrowIOIcon";

/**
 * Will create an instance of the RainHeavyLarge2x TomorrowIO Icon JSX.Element that will create an image
 * tag and return to the as rendered.
 *
 * @package TomorrowIOIcon
 */
class RainHeavyLarge2x extends React.Component {

    /**
     * @inheritDoc
     * @returns {JSX.Element}
     */
    render() {
        return (
            <TomorrowIOIcon
                imgUrl="./src/lib/V2_icons/png/42010_rain_heavy_large@2x.png"
                imgWidth={this.props.imgWidth}
                imgHeight={this.props.imgHeight}
                imgAlt={this.props.imgAlt}
                imgStyle={this.props.imgStyle}
                imgId="tomorrow-io-v2-icons-42010-rain-heavy-large@2x"
            />
        );
    }
}

export default RainHeavyLarge2x;
