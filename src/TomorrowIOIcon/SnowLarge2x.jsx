
/**
 * @author Cristiano Diniz da Silva <@mcloide1977>
 */

import React from "react"
import TomorrowIOIcon from "./TomorrowIOIcon";

/**
 * Will create an instance of the SnowLarge2x TomorrowIO Icon JSX.Element that will create an image
 * tag and return to the as rendered.
 *
 * @package TomorrowIOIcon
 */
class SnowLarge2x extends React.Component {

    /**
     * @inheritDoc
     * @returns {JSX.Element}
     */
    render() {
        return (
            <TomorrowIOIcon
                imgUrl="./src/lib/V2_icons/png/50000_snow_large@2x.png"
                imgWidth={this.props.imgWidth}
                imgHeight={this.props.imgHeight}
                imgAlt={this.props.imgAlt}
                imgStyle={this.props.imgStyle}
                imgId="tomorrow-io-v2-icons-50000-snow-large@2x"
            />
        );
    }
}

export default SnowLarge2x;
