
/**
 * @author Cristiano Diniz da Silva <@mcloide1977>
 */

import React from "react"
import TomorrowIOIcon from "./TomorrowIOIcon";

/**
 * Will create an instance of the FogMostlyClearLarge TomorrowIO Icon JSX.Element that will create an image
 * tag and return to the as rendered.
 *
 * @package TomorrowIOIcon
 */
class FogMostlyClearLarge extends React.Component {

    /**
     * @inheritDoc
     * @returns {JSX.Element}
     */
    render() {
        return (
            <TomorrowIOIcon
                imgUrl="./src/lib/V2_icons/png/21060_fog_mostly_clear_large.png"
                imgWidth={this.props.imgWidth}
                imgHeight={this.props.imgHeight}
                imgAlt={this.props.imgAlt}
                imgStyle={this.props.imgStyle}
                imgId="tomorrow-io-v2-icons-21060-fog-mostly-clear-large"
            />
        );
    }
}

export default FogMostlyClearLarge;
