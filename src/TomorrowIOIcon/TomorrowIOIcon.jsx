/**
 * @author Cristiano Diniz da Silva <@mcloide1977>
 */

import React from "react"
import "./TomorrowIOIcon.css"

/**
 * Will create an instance of the TomorrowIOIcon JSX.Element that will create an image tag and return to the as rendered.
 *
 * @package TomorrowIOIcon
 */
class TomorrowIOIcon extends React.Component{

    /**
     * @inheritDoc
     * @returns {JSX.Element}
     */
    render() {
        return (
            <TomorrowIOIcon>
                <img
                    src={this.props.imgUrl}
                    width={this.props.imgWidth}
                    height={this.props.imgHeight}
                    alt={this.props.imgAlt}
                    style={this.props.imgStyle}
                    id={this.props.imgId}
                />
            </TomorrowIOIcon>
        )
    }
}

export default TomorrowIOIcon;